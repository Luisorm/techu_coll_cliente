require('dotenv').config();
const express = require('express'); //referencia al paquete express
const bodyParser = require('body-parser');
var nodemailer = require('nodemailer');  //  NODEMAILER
var app = express(); // creamos el servidor de Node
app.use(bodyParser.json()); // usamos el body-parser
var puerto = process.env.PORT || 3000;
const cors = require('cors');
app.use(cors());
app.options('*',cors());
const fieldParam = 'f={"_id":0}&';
const sortParam = 's={"transactionID":-1}&';
const URL_BASE = '/api-peru/v1/';
const requestJSON=require('request-json');
const mLabURLbase = process.env.MLAB_URL_BASE;
const apikeyMLab = 'apiKey='+process.env.API_KEY;

// NODEMAILER - ENVIO DE CORREOS
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'constancia.transferencia@gmail.com',
    pass: 'techu2020'
  }
});


// RECUPERA TODOS LOS CLIENTES
app.get(URL_BASE + 'users',
  function(req, res) {
    const httpClient = requestJSON.createClient(mLabURLbase);
    httpClient.get('USER_ACCOUNT?' + fieldParam + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar Cliente de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Cliente no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      });
});

// OBTENER UN SOLO CLIENTE
app.get(URL_BASE + 'users/:id',
  function (req, res) {
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"userID":' + id + '}&';
    var httpClient = requestJSON.createClient(mLabURLbase);
    httpClient.get('USER_ACCOUNT?' + queryString + fieldParam + apikeyMLab,
      function(err, respuestaMLab, body){
        var response = {};
        if(err) {
            response = [{"msg" : "Error obteniendo Cliente."}];
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = [{"msg" : "Cliente no encontrado."}];
            res.status(404);
          }
        }
        res.send(response);
      });
});


// OBTENER CUENTAS DE UN CLIENTE
  app.get(URL_BASE + "users/:id/accounts",
     function(req, res) {
      var httpClient = requestJSON.createClient(mLabURLbase);
      var id=req.params.id;
      var respuesta = {} ;
      var queryString = 'q={"userID":' + id + '}&';
      httpClient.get('USER_ACCOUNT?' + queryString + fieldParam + apikeyMLab,
      function(err, respuestaMLab , body) {
        if(body.length>0){
            var miscuentas = body[0].account;
            respuesta = !err ? miscuentas : [{"msg":"No se pudo recuperar las cuentas del cliente"}];
            res.send(respuesta);
        }else{
            respuesta = [{"msg" : "Cliente no existe"}] ;
            res.send(respuesta);
        }
      });
    });

//CREAR UN NUEVO CLIENTE-CUENTA
app.post(URL_BASE + "cliente_nuevo",
 function(req, res) {
 var httpClient = requestJSON.createClient(mLabURLbase);
 httpClient.get('USER_ACCOUNT?' + apikeyMLab ,
 function(err, respuestaMLab , body) {
    newID=body.length+1;
    console.log("newID:" + newID);
    console.log(req.body.nombres);
    console.log(req.body.apellidos);
    console.log(req.body.cuentas);
    var newUser = {
      "id_cliente" : newID,
      "nombres" : req.body.nombres,
      "apellidos" : req.body.apellidos,
      "correo" : req.body.correo,
      "sexo" : req.body.sexo,
      "contraseña" : req.body.contraseña,
      "cuentas" : req.body.cuentas
      };
      httpClient.post(mLabURLbase + "USER_ACCOUNT?" + apikeyMLab, newUser ,
         function(err, respuestaMLab, body) {
           res.status(200);
           res.send(body);
         });
    });
  });

// ACTUALIZAR CLIENTE
app.put(URL_BASE + "cliente_actualizado/:id",
   function(req, res) {
    var httpClient = requestJSON.createClient(mLabURLbase);
    httpClient.get('USER_ACCOUNT?' + apikeyMLab,
    function(err, respuestaMLab , body) {
       newID=req.params.id;
       console.log(req.body);
       var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
       httpClient.put(mLabURLbase + 'USER_ACCOUNT?q={"userID": ' + newID + '}&' + apikeyMLab, JSON.parse(cambio),
       function(error, respuestaMLab, body) {
         res.send(body);
       });
    });
  });

// AGREGAR NUEVA CUENTA
  app.put(URL_BASE + "users/:id/accounts/new",
     function(req, res) {
      var respuesta = {};
      var httpClient = requestJSON.createClient(mLabURLbase);
      var id=req.params.id;
      var temp = "";
      var ctaMax = 0;
      var queryString = 'q={"userID":' + id + '}&';
      httpClient.get('USER_ACCOUNT?' + queryString + fieldParam + apikeyMLab,
      function(err, respuestaMLab , body) {
          if(body.length==0){
            temp = id.toString()+"101";
            ctaID = parseInt(temp);
          }else{
              var miscuentas = body[0].account;
              var index = 0;
              ctaMax = miscuentas[index].accountID;
              var nroCta = ctaMax;
              for (index in miscuentas){
                nroCta = miscuentas[index].accountID;
                if(nroCta > ctaMax) {
                  ctaMax = nroCta;
                }
              }
              ctaMax = (ctaMax % 1000)+1;
              temp = id.toString()+ctaMax.toString();
              ctaID = parseInt(temp);
          }
          var ofic = Math.floor(Math.random() * 99) + 100;
          var serie = Math.floor(Math.random() * 99999999) + 10000000;
          var plaz = Math.floor(Math.random() * 99) + 100;
          var nro = Math.floor(Math.random() * 999999999) + 100000000;
          var dig = Math.floor(Math.random() * 9) + 10;
          var cuenta = "0011-0"+ ofic.toString() +"-02"+ serie.toString();
          var cci = "011-"+plaz.toString()+"-000"+nro.toString()+"-"+dig.toString();
          var nuevaCta =  {
             "accountID" :   ctaID,
             "description" : req.body.description,
             "cuenta" : cuenta,
             "cci" :    cci,
             "divisa" : req.body.divisa,
             "balance": 0.00
           };
          miscuentas.push(nuevaCta);
          var clienteMod = body[0];
          var cambio = '{"$set":' + JSON.stringify(clienteMod) + '}';
          httpClient.put(mLabURLbase + 'USER_ACCOUNT?q={"userID": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
          function(err, respuestaMLab, body) {
            if(err){
              respuesta = [{"msg":"Error al grabar la cuenta"}];
              res.status(404);
            }else{
              respuesta = [{"msg":"La nueva cuenta se grabó correctamente"}];
              res.status(200);
            }
            res.send(respuesta);
          });
      });
    });

  // ACTUALIZA CUENTA
  app.put(URL_BASE + "users/:id/accounts/update",
         function(req, res) {
          var httpClient = requestJSON.createClient(mLabURLbase);
          var id=req.params.id;
          var queryString = 'q={"userID":' + id + '}&';
          httpClient.get('USER_ACCOUNT?' + queryString + fieldParam + apikeyMLab,
          function(err, respuestaMLab , body) {
             var miscuentas = body[0].account;
             var id_cta = req.body.accountID;
             var nuevaCta = {
               "accountID" : id_cta,
               "description" : req.body.description,
               "cuenta" : req.body.cuenta,
               "CCI" :    req.body.cci,
               "divisa" : req.body.divisa,
               "balance": req.body.balance
               };
            var cta_index = 0;
            var existe = false;
            for (cta_index in miscuentas){
              if(miscuentas[cta_index].accountID == id_cta) {
                existe = true;break;
              }
            }
            if (!existe){
              let respuesta = [{"msg": "Cuenta No existe"}];
              res.send(respuesta);
            }
            else{
              miscuentas.splice(cta_index,1,nuevaCta);
              var clienteMod = body[0];
              var cambio = '{"$set":' + JSON.stringify(clienteMod) + '}';
              httpClient.put(mLabURLbase + 'USER_ACCOUNT?q={"userID": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
              function(err, respuestaMLab, body) {
                var respuesta = {} ;
                respuesta = !err ? [{"msg":"Actualizacion correcta"}] : [{"msg":"Error al Actualizar en mlab"}];
                res.send(respuesta);
              });
            }
      });
  });


// RECUPERA TODAS LAS TRANSACCIONES DE UNA CUENTA
    app.get(URL_BASE + "accounts/:id/transactions",
          function(req, res) {
              var httpClient = requestJSON.createClient(mLabURLbase);
              var id=req.params.id;
              var respuesta = {} ;
              var queryString = 'q={"accountID":' + id + '}&';
              httpClient.get('TRANSACTION?' + queryString + fieldParam + apikeyMLab,
              function(err, respuestaMLab , body) {
                  if(body.length>0){
                     respuesta = body;
                  }else{
                    res.status(404);
                    respuesta = [{"msg":"La cuenta no tiene movimientos"}];
                  }
                  res.send(respuesta);
               });
          });


//  RECUPERA LOS DATOS DE UN CLIENTE A PARTIR DE SU CUENTA
app.post(URL_BASE + "accounts/users",
      function(req, res) {
          var httpClient = requestJSON.createClient(mLabURLbase);
          var cuenta=req.body.cuenta;
          var tipo = req.body.tipo;
          httpClient.get('USER_ACCOUNT?' + fieldParam + apikeyMLab,
          function(err, respuestaMLab , body) {
               var usuarios = body;
               var cliente = {};
               var indexRes = getIndexUserAccount(usuarios, cuenta, tipo);
               var existe = indexRes[0];
               var user_index = indexRes[1];
               var cta_index = indexRes[2];
               var respuesta = {};
               if(existe==1){
                 cliente = [{
                    "id_cliente" : usuarios[user_index].userID,
                    "first_name" : usuarios[user_index].first_name,
                    "last_name" : usuarios[user_index].last_name,
                    "email" : usuarios[user_index].email,
                    "sexo" : usuarios[user_index].sexo,
                    "accountID" : miscuentas[cta_index].accountID,
                    "description" : miscuentas[cta_index].description,
                    "cuenta" : miscuentas[cta_index].cuenta,
                    "cci" :    miscuentas[cta_index].cci,
                    "divisa" : miscuentas[cta_index].divisa,
                    "balance": miscuentas[cta_index].balance
                 }];
                  respuesta = cliente;
                  res.status(200);
               }else{
                 respuesta = [{"msg":"Cuenta/CCI no encontrado"}];
                 res.status(404);
               }
               res.send(respuesta);
          });
   });

function getIndexUserAccount(users, cuenta, tipo){
       var usuarios = users;
       var user_index = 0;
       var cta_index = 0;
       var existe = 0;
       if (tipo==1){
           for (user_index in usuarios){
             miscuentas = usuarios[user_index].account;
             cta_index = 0;
             for (cta_index in miscuentas){
               if(miscuentas[cta_index].cuenta == cuenta) {
                    existe = 1;
                    break;
               }
            }
            if(existe) break;
          }
       }else{
          for (user_index in usuarios){
            miscuentas = usuarios[user_index].account;
            cta_index = 0;
            for (cta_index in miscuentas){
              if(miscuentas[cta_index].cci == cuenta) {
                   existe = 1;
                   break;
              }
           }
           if(existe) break;
         }
       }
       var index = [existe,user_index,cta_index];
       return index;
   }


// TRANSFERENCIA DE CUENTA A CUENTA
  app.put(URL_BASE + "users/:id/transactions",
          function(req, res) {
              var httpClient = requestJSON.createClient(mLabURLbase);
              var respuesta = {};
              var tipoCambio = 3.45;
              httpClient.get('USER_ACCOUNT?' + fieldParam + apikeyMLab,
              function(err, respuestaMLab , body) {
                  if(body.length > 0) {
                     var usuarios = body;
                     var iban_ord = req.body.cta_ord;
                     var indexOrd = getIndexUserAccount(usuarios, iban_ord, 1);
                     var user_index = indexOrd[1];
                     var miscuentas = usuarios[user_index].account;
                     var montoTrans = req.body.monto;
                     var montoAbo = req.body.monto;
                     var divisaOper = req.body.divisa;
                     var email = usuarios[user_index].email;
                     var nombOrd = usuarios[user_index].first_name+' '+usuarios[user_index].last_name;
                     var cta_index = indexOrd[2];
                     var existe = indexOrd[0];
                     var balance = miscuentas[cta_index].balance;
                     var ctaID = miscuentas[cta_index].accountID;
                     var divisaCta = miscuentas[cta_index].divisa;
                     var userID = usuarios[user_index].userID;
                     var signoDiv = '';
                      if(existe==0){
                          respuesta = [{"msg":"Cuenta ordenante no existe"}];
                          res.status(404);
                          res.send(respuesta);
                      }else{
                          if(divisaOper=='USD' && divisaCta=='PEN'){
                             montoTrans *= tipoCambio;
                             sigDiv = 'S/ ';
                             sigDivOp = '$ ';
                          }else if (divisaOper=='PEN' && divisaCta=='USD') {
                                    montoTrans /= tipoCambio;
                                    sigDiv = '$ ';
                                    sigDivOp = 'S/ ';
                          }
                          if (balance < montoTrans){
                              respuesta = [{"msg":"Saldo insuficiente"}];
                              res.status(404);
                              res.send(respuesta);
                          }else{
                              montoTrans = Math.round(montoTrans*100)/100;
                              balance -= montoTrans;
                              miscuentas[cta_index].balance = Math.round(balance*100)/100;
                              var cuentaMod = usuarios[user_index];
                              var cambio = '{"$set":' + JSON.stringify(cuentaMod) + '}';
                              httpClient.put(mLabURLbase + 'USER_ACCOUNT?q={"userID": ' + userID + '}&' + apikeyMLab, JSON.parse(cambio),
                                 function(err, respuestaMLab, body) {
                                    if(err){
                                        respuesta =  {"msg":"Error al actualizar Cuenta ordenante"};
                                        res.status(404);
                                        res.send(respuesta);
                                    }else{
                                    // Párrafo para realizar el Abono a la cuenta de beneficiario
                                        var iban_ben = req.body.cta_ben;
                                        var tipo = req.body.tipo;
                                        var indexBen = getIndexUserAccount(usuarios, iban_ben, tipo);
                                        var user_indexB = indexBen[1];
                                        var cta_indexB = indexBen[2];
                                        var existeB = indexBen[0];
                                        if(existeB==0){
                                               respuesta = [{"msg":"Cuenta beneficiaria no existe"}];
                                               res.status(404);
                                               res.send(respuesta);
                                        }else{
                                            var miscuentasB = usuarios[user_indexB].account;
                                            var nombBen = usuarios[user_indexB].first_name+' '+usuarios[user_indexB].last_name;
                                            var balanceB = miscuentasB[cta_indexB].balance;
                                            var ctaIDB = miscuentasB[cta_indexB].accountID;
                                            var divisaCtaB = miscuentasB[cta_indexB].divisa;
                                            var userIDB = usuarios[user_indexB].userID;
                                            balanceB += montoAbo;
                                            miscuentasB[cta_indexB].balance = Math.round(balanceB*100)/100;
                                            var cuentaAbo = usuarios[user_indexB];
                                            var cambioB = '{"$set":' + JSON.stringify(cuentaAbo) + '}';
                                            httpClient.put(mLabURLbase + 'USER_ACCOUNT?q={"userID": ' + userIDB + '}&' + apikeyMLab, JSON.parse(cambioB),
                                               function(err, respuestaMLab, body) {
                                                 if(err){
                                                     respuesta =  {"msg":"Error al actualizar Cuenta beneficiaria"};
                                                     res.status(404);
                                                     res.send(respuesta);
                                                 }else{
                                                      httpClient.get('TRANSACTION?' + sortParam + fieldParam + apikeyMLab,
                                                      function(err, respuestaMLab , bodyT) {
                                                         var transID = bodyT[0].transactionID;
                                                         var newTrans = postTransaction(transID,ctaID,req);
                                                         httpClient.post(mLabURLbase + "TRANSACTION?" + apikeyMLab, newTrans ,
                                                            function(errorT, respuestaMLab, body) {
                                                              if(errorT){
                                                                 respuesta = [{"msg": "Error en la transferencia"}];
                                                                 res.send(respuesta);
                                                              }else{
                                                                 enviarEmail(nombOrd,iban_ord,nombBen,email,montoTrans,sigDiv,sigDivOp,req);
                                                                 respuesta = [{"msg": "Transferencia realizada correctamente"}];
                                                                 res.status(200);
                                                                 res.send(respuesta);
                                                              }
                                                         });
                                                     });
                                                 }
                                            });
                                      }
                                  }
                              });
                           }
                       }
                     }else{
                         respuesta = [{"msg" : "Cliente no existe"}];
                         res.send(respuesta);
                     }
                });
        });


// FUNCION PARA GRABAR UNA TRANSACCIONES
 function postTransaction(transID, ctaID, req){
      var hoy = new Date();
      var dd = hoy.getDate();
      var mm = hoy.getMonth()+1;
      var yyyy = hoy.getFullYear();
      if(dd<10){dd = '0'+dd;}
      if(mm<10){mm = '0'+mm;}
      var fechaOper = dd+"/"+mm+"/"+yyyy;
      transID+=1;
      var newTrans = {
          "transactionID" : transID,
          "description" : req.body.description,
          "fecha" :    fechaOper,
          "divisa" : req.body.divisa,
          "monto": req.body.monto,
          "tipo" : "Cargo",
          "accountID" : ctaID,
          "cuenta": req.body.cta_ben
       };
       return newTrans;
 }


 function enviarEmail(nombOrd,iban_ord,nombBen,email,montoTrans,signoDiv,sigDivOp,req){
     var mailOptions = {
       from: 'constancia.transferencia@gmail.com',
       to: JSON.stringify(email),
       subject: 'BANCA DIGITAL - Constancia de Transferencia',
       html: '<h1>CONSTANCIA DE TRANSFERENCIA</h1>'+
             '<p><h2>TechU 2020</h2></p>'+
             '<p><h3>Cliente origen: '+nombOrd+'</h3></p>'+
             '<p><h3>Cuenta origen: '+iban_ord+'</h3></p>'+
             '<p><h3>Cliente destino: '+nombBen+'</h3></p>'+
             '<p><h3>Cuenta destino: '+req.body.cta_ben+'</h3></p>'+
             '<p><h3>Concepto: '+req.body.description+'</h3></p>'+
             '<p><h3>Importe transferido: '+sigDivOp+req.body.monto+'</h3></p>'+
             '<p><h3>Divisa: '+req.body.divisa+'</h3></p>'+
             '<p><h3>Importe cargado: '+sigDiv+montoTrans+'</h3></p>'
     };
     transporter.sendMail(mailOptions, function(error, info){
       if (error) {
         console.log(error);
       } else {
         console.log('Email enviado: ' + info.response);
       }
     });
 }


  // ELIMINA CUENTA
   app.put(URL_BASE + "users/:id/accounts/delete",
         function(req, res) {
          var httpClient = requestJSON.createClient(mLabURLbase);
          var id=req.params.id;
          var queryString = 'q={"userID":' + id + '}&';
          httpClient.get('USER_ACCOUNT?' + queryString + fieldParam + apikeyMLab,
          function(err, respuestaMLab , body) {
            var miscuentas = body[0].account;
            var id_cta = req.body.accountID;
            console.log(id_cta);
            var cta_index = 0;
            var existe = false;
            for (cta_index in miscuentas){
              if(miscuentas[cta_index].accountID== id_cta) {
                existe = true;break;
              }
            }
            if (!existe){
              let respuesta = [{"msg": "Cuenta No existe"}];
              res.send(respuesta);
            }
            else{
              miscuentas.splice(cta_index,1);
              var clienteMod = body[0];
              var cambio = '{"$set":' + JSON.stringify(clienteMod) + '}';
              httpClient.put(mLabURLbase + 'USER_ACCOUNT?q={"userID": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
              function(error, respuestaMLab, body) {
                  let respuesta = [{"msg": "Cuenta eliminada"}];
                  res.send(respuesta);
              });
            }
          });
        });


  //Method POST login - FRED
  app.post(URL_BASE + "login",
    function (req, res){
      var httpClient = requestJSON.createClient(mLabURLbase);
      var email= req.body.email;
      var pass= req.body.password;
      var queryStringEmail='q={"email":"' + email + '"}&';
      var queryStringpass='q={"password":' + pass + '}&';
      httpClient.get('USER_ACCOUNT?'+ queryStringEmail + apikeyMLab ,
      function(error, respuestaMLab , body) {
        var respuesta = body[0];
        if(respuesta!=undefined){
            if (respuesta.password == pass) {
              if(respuesta.logged==true){
                res.status(400);
                res.send([{"msg":"El usuario ya inició sesion"}]);
              }else{
                var session={"logged":true};
                var login = '{"$set":' + JSON.stringify(session) + '}';
                httpClient.put('USER_ACCOUNT?q={"userID": ' + respuesta.userID + '}&' + apikeyMLab, JSON.parse(login),
                 function(errorP, respuestaMLabP, bodyP) {
                  getUserId(respuesta.userID,res);
                });
              }
            }
            else {
              res.status(400);
              res.send([{"msg":"contraseña incorrecta"}]);
            }
        }else{
          res.status(400);
          res.send([{"msg": "email Incorrecto"}]);
        }
      });
  });


  //Method POST logout
  app.post(URL_BASE + "logout",
    function (req, res){
      var email= req.body.email;
      var queryStringEmail='q={"email":"' + email + '"}&';
      var httpClient = requestJSON.createClient(mLabURLbase);
      httpClient.get('USER_ACCOUNT?'+ queryStringEmail + apikeyMLab ,
      function(error, respuestaMLab , body) {
         var respuesta = body[0];
         if(respuesta!=undefined){
              if(respuesta.logged!=true){
                res.status(400);
                res.send({"msg": "El usuario no ha iniciado sesion"});
              }else{
                var session={"logged":true};
                var logout = '{"$unset":' + JSON.stringify(session) + '}';
                httpClient.put('USER_ACCOUNT?q={"userID": ' + respuesta.userID + '}&' + apikeyMLab, JSON.parse(logout),
                function(errorP, respuestaMLabP, bodyP) {
                  getUserId(respuesta.userID,res);
                });
              }
          }else{
            res.status(400);
              res.send({"msg": "Error en logout"});
          }
      });
  });

  //  RECUPERA LOS DATOS DE UN CLIENTE A PARTIR DE SU CUENTA
  app.get(URL_BASE + "accounts/users",
        function(req, res) {
            var httpClient = requestJSON.createClient(mLabURLbase);
            var cuenta=req.body.cuenta;
            var tipo = req.body.tipo;
            httpClient.get('USER_ACCOUNT?' + fieldParam + apikeyMLab,
            function(err, respuestaMLab , body) {
                 var usuarios = body;
                 var cliente = {};
                 var user_index = 0;
                 var cta_index = 0;
                 var userID = 0;
                 var existe = false;
                 if (tipo==1){
                     for (user_index in usuarios){
                       miscuentas = body[user_index].account;
                       cta_index = 0;
                       for (cta_index in miscuentas){
                         if(miscuentas[cta_index].cuenta == cuenta) {
                              existe = true;
                              break;
                         }
                      }
                      if(existe) break;
                    }
                 }else{
                    for (user_index in usuarios){
                      miscuentas = body[user_index].account;
                      cta_index = 0;
                      for (cta_index in miscuentas){
                        if(miscuentas[cta_index].cci == cuenta) {
                             existe = true;
                             break;
                        }
                     }
                     if(existe) break;
                   }
                 }
                 cliente = [{
                    "id_cliente" : usuarios[user_index].userID,
                    "first_name" : usuarios[user_index].first_name,
                    "last_name" : usuarios[user_index].last_name,
                    "email" : usuarios[user_index].email,
                    "sexo" : usuarios[user_index].sexo,
                    "accountID" : miscuentas[cta_index].accountID,
                    "description" : miscuentas[cta_index].description,
                    "cuenta" : miscuentas[cta_index].cuenta,
                    "cci" :    miscuentas[cta_index].cci,
                    "divisa" : miscuentas[cta_index].divisa,
                    "balance": miscuentas[cta_index].balance
                 }];
                 var respuesta = {};
                 if(existe){
                    respuesta = cliente;
                    res.status(200);
                 }else{
                   respuesta = [{"msg":"Cuenta/CCI no encontrado"}];
                   res.status(404);
                 }
                 res.send(respuesta);
            });
        });


   function getUserId(ind,res){
        const httpClient = requestJSON.createClient(mLabURLbase);
        const fieldParam = 'f={"_id":0}&';
        let queryString = 'q={"userID":'+ind+'}&';
        httpClient.get('USER_ACCOUNT?'+queryString + fieldParam+ apikeyMLab,
          function(err, respuestaMLab, body) {
            var response = {};
            if(err) {
              response = [{"msg" : "Error al recuperar users de mLab."}]
              res.status(500);
          } else {
            if(body.length > 0) {
              var usuario = [{
                "userID": ind,
                "first_name": body[0].first_name,
                "last_name": body[0].last_name,
                "email": body[0].email,
              }]
              response = usuario;
            } else {
              response = [{"msg" : "Usuario no encontrado."}];
              res.status(404);
            }
          }
          res.send(response);
        });
    }


app.listen(puerto,function(){
  console.log('Node JS escuchando puerto: 3000');
});
